#!/usr/bin/env python
# coding: utf-8

# # DeepDream

# In[ ]:





# In[4]:


from __future__ import print_function
import numpy as np
from keras import layers
from keras import models
from keras import optimizers
from keras.models import load_model
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator
from keras.applications import VGG16
from keras.applications import inception_v3
from keras import backend as K
from keras.callbacks import ModelCheckpoint
import matplotlib.pyplot as plt
from matplotlib.pyplot import rcParams
import imageio
import os


# In[6]:


# Listing 8.8 Loading the pretrained Inception V3 model

# You won't be training the model, so this command disables all training specific operations
K.set_learning_phase(0)

model = inception_v3.InceptionV3(
                                weights="imagenet",
                                include_top=False)


# In[8]:


# show architecture
#model.summary()


# In[ ]:


# Listing 8.9 Setting up the DeepDream configuration

# Dictionary mapping layer names to a coefficient quantifying how much the layer's activation contributes to the loss you will seek
# to maximize. Note that the layer names are hardcoded in the built-in Inception V3 application. You can list all layer names using model.summary()
layer_contributions = {
    "mixed2": 0.2,
    "mixed3": 3.0,
    "mixed4": 2.0,
    "mixed5": 1.5,
}


# In[ ]:


# Listing 8.10 Defining the loss to be maximized
layer_dict = dict([(layer.name, layer) for layer in model.layers])

# We define the loss by adding layer contributions to this scalar variable
loss = K.variable(0.)

for layer_name in layer_contributions:
    
    coeff = layer_contributions[layer_name]
    
    activation = layer_dict[layer_name].output                         # check tensor shape
    
    scaling = K.prod(K.cast(K.shape(activation), "float32"))           # check
    
    # activation[n_samples, height, width, depth]
    # activation[:, 2:-2, 2:-2, :] means "take all the images and remove the border pixels" 
    loss = loss + coeff * K.sum(K.square(activation[:, 2:-2, 2:-2, :])) / scaling                      # check
    
    # loss: adds the L2 norm (i.e., sum of squares) of the features of a layer to the loss.
    # You can avoid border artifacts by only involving nonborder pixels in the loss.


# In[ ]:


# Listing 8.11 Gradient-ascent process

# This tensor holds the generated image: the dream
dream = model.input

# Computes the gradients of the dream with regard to the loss
grads = K.gradients(loss, dream)[0]

# Normalizes the gradients (important trick)
grads /= K.maximum(K.mean(K.abs(grads)), 1e-7)

# Sets up a Keras function to retrieve the value of the loss and gradients,
# given an input image
outputs = [loss, grads]
fetch_loss_and_grads = K.function([dream], outputs)

def eval_loss_and_grads(x):
    outs = fetch_loss_and_grads([x])
    loss_value = outs[0]
    grad_values = outs[1]
    return loss_value, grad_values

def gradient_ascent(x, iterations, step, max_loss=None):
    # This function runs gradient ascent for a number of iterations
    for i in range(iterations):
        loss_value, grad_values = eval_loss_and_grads(x)
        if max_loss is not None and loss_value > max_loss:
            break
        print("...Loss value at %d: %.5f" % (i, loss_value))
        x += step * grad_values
    return x


# In[ ]:


# Listing 8.13 Auxiliary functions
import scipy

def resize_img(img, size):
    
    img = np.copy(img)
    
    # scaling factors for each dimension
    # (n_samples, height, width, channels)
    # only height and width are scaled
    factors = (1,
               float(size[0]) / img.shape[1],
               float(size[1]) / img.shape[2],
               1
              )
    
    return scipy.ndimage.zoom(img, factors, order=1)


def save_img(img, fname):
    pil_img = deprocess_image(np.copy(img))
    
    # deprecated
    #scipy.misc.imsave(fname, pil_img)
    
    imageio.imsave(fname, pil_img)
    
    print("output", fname)
    
def preprocess_image(image_path):
    # Util function to open, resize, and format pictures into tensors that Inception V3 can process
    img = image.load_img(image_path)
    img = image.img_to_array(img)
    img = np.expand_dims(img, axis=0)
    img = inception_v3.preprocess_input(img)    # check
    return img

def deprocess_image(x):
    # Util function to convert a tensor into a valid image
    if K.image_data_format() == "channels_first":
        x = x.reshape((3, x.shape[2], x.shape[3]))
        x = x.transpose((1, 2, 0))
    else:
        # Undoes preprocessing that was performed by inception_v3.preprocess_input
        x = x.reshape((x.shape[1], x.shape[2], 3))
        
    x /= 2.0
    x += 0.5
    x *= 255.
    x = np.clip(x, 0, 255).astype("uint8")
    return x


# In[ ]:


# Listing 8.12

# Playing with these hyperparameters will let you achieve new effects
step = 0.01             # Gradient ascent step size
num_octave = 3          # Number of scales at which to run gradient ascent
octave_scale = 1.4      # Size ratio between scales
iterations = 20         # Number of ascent steps to run at each scale

max_loss = 10.0         # If the loss grows larger than 10, we interrupt the gradient-ascent process to avoid ugrly artifacts

base_image_path = "sheldon_square.jpg" # Fill this with the path to the image you want to use

img = preprocess_image(base_image_path)

# Prepares a list of shape tuples defining the different scales at which to run gradient ascent
original_shape = img.shape[1:3]
successive_shapes = [original_shape]
for i in range(1, num_octave):
    shape = tuple([int(dim / (octave_scale ** i)) for dim in original_shape])
    successive_shapes.append(shape)

# print shapes

# Reverses the list of shapes so they are in increasing order
successive_shapes = successive_shapes[::-1]

# Resizes the numpy array of the image to the smallest scale
original_img = np.copy(img)
shrunk_original_img = resize_img(img, successive_shapes[0])

for shape in successive_shapes:
    print("Processing image shape", shape)
    
    # Scales up the dream image
    img = resize_img(img, shape)
    
    # Runs gradient ascent, altering the dream
    img = gradient_ascent(img,
                         iterations=iterations,
                         step=step,
                         max_loss=max_loss)
    
    # Scales up the smaller version of the original image: it will be pixellated
    upscaled_shrunk_original_img = resize_img(shrunk_original_img, shape)
    
    # Computes the high-quality version of the original image at this size (ie, resizes the original image)
    same_size_original = resize_img(original_img, shape)
    
    # The difference between the two is the detail that was lost when scaling up
    lost_detail = same_size_original - upscaled_shrunk_original_img
    
    # Reinject lost detail into the dream
    img += lost_detail
    
    # Reset
    shrunk_original_img = resize_img(original_img, shape)
    
    save_img(img, fname="dream_at_scale_" + str(shape) + ".png")
    
save_img(img, fname="final_dream.png")

