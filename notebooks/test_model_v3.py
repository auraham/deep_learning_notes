# test_model_v3.py
import os
import numpy as np
from keras.models import load_model
from keras.preprocessing.image import ImageDataGenerator

# --- Load model ---
model = load_model("dogs_vs_cats_v3_callback.h5")

# --- Evaluation ---
test_datagen = ImageDataGenerator(rescale=1./255)    # we multiply the data by the value provided (after applying all other transformations)

test_path = "/home/auraham/datasets/dogs_vs_cats_small/test"
test_generator = test_datagen.flow_from_directory(test_path,                     # target directory
                                                 target_size=(150, 150),         # resizes all images to (150, 150), they are still color images
                                                 batch_size=20,                  # number of images on each batch
                                                 class_mode="binary",            # becuase you use binary_crossentropy, you need binary labels
                                                 )

# evaluate model (actualizar)
test_loss, test_acc = model.evaluate_generator(test_generator,
                                              steps=1000/20)
print("test acc: %.4f" % test_acc)
