# Deep learning notes

This repository contains my notes about machine learning and deep learning.



## Recommended lectures

- [The state of Computer Vision and AI: we are really, really far away](http://karpathy.github.io/2012/10/22/state-of-computer-vision/). In this post, Andrej Karpathy explains many of the challenges for computer vision using a single image.
- [Building a Pokedex in Python](https://www.pyimagesearch.com/2014/05/19/building-pokedex-python-comparing-shape-descriptors-opencv/). Adrian Rosenbrock provides a step-by-step tutorial for creating a classifier (pokedex) that can visually recognize Pokemon from the original games for the Game Boy. To this end, his pipeline involves OpenCV for preprocessing and feature extraction and a simple k-NN as classifier.
- [Colah' Blog](http://colah.github.io/). A lot of interesting posts about neural networks, backpropagation, long-short term memory, inception, etc.



## References

- [Hands-On Machine Learning with Scikit-Learn and TensorFlow](https://www.oreilly.com/library/view/hands-on-machine-learning/9781491962282/). In this book, Aurélien Géron gives an intuitive explanation of a range of techniques. The first part of the book is mainly focused on the fundamentals of machine learning (linear regression, support vector machines, decision trees, ensemble learning, random forest, etc) using scikit-learn. The second part relies on deep learning with TensorFlow. You can also get the notebooks in [github](https://github.com/ageron/handson-ml).
- [Deep Learning with Python](https://www.manning.com/books/deep-learning-with-python). This books was written by Keras creator François Chollet. Most of my notes are taken from this book. With this book, you will learn how to use Keras for creating networks very easily. The second edition of the book is on the go.
- [Neural Networks and Deep Learining](http://neuralnetworksanddeeplearning.com/). Michael Nielsen introduces the building blocks of shallow and deep neural networks. I recommend this book as a first introduction to convolution neural networks.
- [Deep Learning with Pytorch](https://www.manning.com/books/deep-learning-with-pytorch?query=deep%20learning) At the time of writing, this book is still unfinished. Although, you can [get a free copy](https://pytorch.org/deep-learning-with-pytorch) of the first chapters.
- [Deep Learning for Computer Vision](https://www.pyimagesearch.com/deep-learning-computer-vision-python-book/). This book gives a concise history of neural networks and deep learning, describes architectures (like LeNet, VGG16, ResNet50, Inception V3, and Xception), and many other concepts. 
- [Deep Learning and the  Game of Go](https://www.manning.com/books/deep-learning-and-the-game-of-go). In this book, Max Pumperla and Kevin Ferguson explain how to create a Go bot called BetaGo. Unlike other books that focus on many areas/applications of deep learning, this book explores many algorithms for solving a single task: how to play Go. 
- [The Hundred-Page Machine Learning Book](http://themlbook.com/). This book gives a brief description of different machine learning areas. You can [get the first chapters here](http://themlbook.com/wiki/doku.php).
- [CS231n Convolutional Neural Networks for Visual Recognition](http://cs231n.github.io/). Notes of the visual recognition course from Stanford.



## Additional links

- [The limitations of deep learning](https://blog.keras.io/the-limitations-of-deep-learning.html)
- Fooling neural networks:
  - [These glasses trick facial recognition software into thinking you're someone else](https://www.theverge.com/2016/11/3/13507542/facial-recognition-glasses-trick-impersonate-fool) [[paper](https://www.cs.cmu.edu/~sbhagava/papers/face-rec-ccs16.pdf)]
  - [Adversarial Patch](https://arxiv.org/pdf/1712.09665.pdf) [medium](https://medium.com/deep-dimension/deep-learning-papers-review-universal-adversarial-patch-a5ad222a62d2) [medium](https://towardsdatascience.com/lets-fool-a-neural-network-b1cded8c4c07)
  - [Deep Neural Networks are Easily Fooled: High Confidence Predictions for Unrecognizable Images](http://www.evolvingai.org/fooling)
  - [Pixel Deflection](https://iamaaditya.github.io/2018/02/demo-for-pixel-deflection/)
- [VGG Convolutional Neural Networks Practical](http://www.robots.ox.ac.uk/~vgg/practicals/cnn/index.html)
- [Building poweful image classification models using very little data](https://blog.keras.io/building-powerful-image-classification-models-using-very-little-data.html)
- [Feature importances with forests of trees](https://scikit-learn.org/stable/auto_examples/ensemble/plot_forest_importances.html)
- [Convolutional Neural Networks Cheatsheet](https://stanford.edu/~shervine/teaching/cs-230/cheatsheet-convolutional-neural-networks)



## Contact

Auraham Camacho `auraham.cg@gmail.com`